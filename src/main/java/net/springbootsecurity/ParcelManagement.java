package net.springbootsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ParcelManagement
{
	public static void main(String[] args)
	{
		SpringApplication.run(ParcelManagement.class, args);
	}

}
