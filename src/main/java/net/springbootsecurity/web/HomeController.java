package net.springbootsecurity.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import net.springbootsecurity.entities.Message;
import net.springbootsecurity.repositories.MessageRepository;

@Controller
public class HomeController {
	@Autowired
	private MessageRepository messageRepository;

	@GetMapping("/home")
	public String home(Model model) {
		model.addAttribute("msgs", messageRepository.findAll());
		return "userHome";
	}

	@PostMapping("/messages")
	public String saveMessage(Message message) {
		messageRepository.save(message);
		return "redirect:/home";
	}
	@PostMapping("/messages/{id}")
	public String updateMessage(@PathVariable int id, Message message) {
		messageRepository.findById(id);
		messageRepository.save(message);
		return "redirect:/home";
	}
	@DeleteMapping("/messages/{id}")
	public String deleteMessage(@PathVariable int id) {
		messageRepository.deleteById(id);
		return "redirect:/home";
	}
}
