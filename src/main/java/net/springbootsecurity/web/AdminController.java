package net.springbootsecurity.web;

import net.springbootsecurity.entities.User;
import net.springbootsecurity.repositories.InfoRepository;
import net.springbootsecurity.repositories.UserRepository;
import net.springbootsecurity.repositories.VacationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@Controller
public class AdminController {
    @Autowired
    UserRepository userRepository;

    @Autowired
    InfoRepository infoRepository;

    @Autowired
    VacationRepository vacationRepository;

    @GetMapping("/admin/home")
    public String home(Model model) {
        model.addAttribute("users", userRepository.findAll());
        return "adminHome";
    }
    @PostMapping("/admin/home/")
    public String saveUser(User user) {
        userRepository.save(user);
        return "redirect:/admin/home";
    }
    @PostMapping("/admin/home/{id}")
    public String getUser(@PathVariable int id, User user) {
        if(userRepository.findById(id).isPresent()) {
            userRepository.findById(id);
            userRepository.save(user);
        }
        return "redirect:/admin/home";
    }

    @DeleteMapping("/admin/home/{id}")
    public String deleteUser(@PathVariable int id) {
        userRepository.deleteById(id);
        vacationRepository.deleteById(id);
        infoRepository.deleteById(id);
        return "redirect:/admin/home";
    }

    @GetMapping("/admin/home/findByFirstName")
    public String getUserByFirstName(@RequestParam(value = "name") String firstName, Model model) {
        List<User> userInfo = userRepository.findUserByFirstName(firstName);

        model.addAttribute("findByFirstName", userInfo);

        return "findByFirstName";
    }

    @GetMapping("/admin/home/findByLastName")
    public String getUserByLastName(@RequestParam(value = "name") String lastName, Model model) {
        List<User> userInfo = userRepository.findUserByLastName(lastName);

        model.addAttribute("findByLastName", userInfo);

        return "findByLastName";
    }

    @GetMapping("/admin/home/findBySSN")
    public String getUserBySSN(@RequestParam(value = "name") String SSN, Model model) {
        User userInfo = userRepository.findUserBySSN(SSN);

        model.addAttribute("findBySSN", userInfo);

        return "findBySSN";
    }

    @GetMapping("/admin/home/findByEmail")
    public String getUserByEmail(@RequestParam(value = "name") String email, Model model) {
        User userInfo = userRepository.findUserByEmail(email);
        model.addAttribute("findByEmail", userInfo);

        return "findByEmail";
    }

    @GetMapping("/admin/home/findBySalary")
    public String getUserBySalary(@RequestParam(value = "name") double salary, Model model) {
        User userInfo = userRepository.findUserBySalary(salary);
        model.addAttribute("findBySalary", userInfo);

        return "findBySalary";
    }

    @GetMapping("/admin/home/findByBirthDay")
    public String getUsersByBirthday(@RequestParam(value = "name")
                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate birthday, Model model) {
        User userInfo = userRepository.findUserByBirthday(birthday);
        model.addAttribute("findByBirthday", userInfo);

        return "findByBirthDay";
    }

    @GetMapping("/admin/home/usersByFirstName")
    public String getUsersByFirstName(@RequestParam(value = "name")String firstName, Model model) {
        List<User> userList = userRepository.findUsersByFirstName(firstName);
        model.addAttribute("usersByFirstName", userList);
        return "usersByFirstName";
    }

    @GetMapping("/admin/home/usersByLastName")
    public String getUsersByLastName(@RequestParam(value = "name")String lastName, Model model) {
        List<User> userList = userRepository.findUsersByLastName(lastName);
        model.addAttribute("usersByLastName", userList);
        return "usersByLastName";
    }

    @GetMapping("/admin/home/usersTillSalary")
    public String getUsersTillSalary(@RequestParam(value = "name")double salary, Model model) {
        List<User> userList = userRepository.findUsersTillSalary(salary);
        model.addAttribute("usersTillSalary", userList);
        return "usersTillSalary";
    }

    @GetMapping("/admin/home/usersTillBirthday")
    public String getUsersByFirstName(@RequestParam(value = "name")
                                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)LocalDate birthday, Model model) {
        List<User> userList = userRepository.findUsersTillBirthday(birthday);
        model.addAttribute("usersTillBirthday", userList);
        return "usersTillBirthday";
    }

}
