package net.springbootsecurity.repositories;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import net.springbootsecurity.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	@Query(value = "SELECT users FROM User users WHERE users.information.id = users.id AND users.id IN ?1")
	List<User> userById(int id);

	Optional<User> findByEmail(String email);

	@Query(value = "SELECT users.information.user FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.firstName IN ?1")
	List<User> findUserByFirstName(String firstName);
	@Query(value = "SELECT users.information.user FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.lastName IN ?1")
	List<User> findUserByLastName(String lastName);
	@Query(value = "FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.SSN IN ?1")
	User findUserBySSN(String SSN);
	@Query(value = "FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.user.email IN ?1")
	User findUserByEmail(String email);
	@Query(value = "FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.salary = ?1")
	User findUserBySalary(double salary);
	@Query(value = "FROM User users INNER JOIN users.information infos INNER JOIN infos.user userss WHERE infos.birthday = ?1")
	User findUserByBirthday(LocalDate birthday);

	@Query(value = "FROM User users WHERE users.id IN (SELECT infos.user.id FROM users.information infos WHERE infos.firstName = ?1)")
	List<User> findUsersByFirstName(String firstName);
	@Query(value = "FROM User users WHERE users.id IN (SELECT infos.user.id FROM users.information infos WHERE infos.lastName = ?1)")
	List<User> findUsersByLastName(String lastName);
	@Query(value = "FROM User users WHERE users.id IN (SELECT infos.user.id FROM users.information infos WHERE infos.salary < ?1)")
	List<User> findUsersTillSalary(double salary);
	@Query(value = "FROM User users WHERE users.id IN (SELECT infos.user.id FROM users.information infos WHERE infos.birthday < ?1)")
	List<User> findUsersTillBirthday(LocalDate birthday);
}
