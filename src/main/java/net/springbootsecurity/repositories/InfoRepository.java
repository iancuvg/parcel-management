package net.springbootsecurity.repositories;

import net.springbootsecurity.entities.Info;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface InfoRepository extends JpaRepository<Info, Integer> {
//    List<Info> findByOrderByBirthdayAsc(LocalDate birthday);
//    List<Info> findByOrderByBirthdayDesc(LocalDate birthday);
    @Query(value = "FROM User users INNER JOIN users.information infos WHERE infos.firstName IN ?1")
    List<Info> findInfosByFirstName(String firstName);
}
