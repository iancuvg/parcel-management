package net.springbootsecurity.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import net.springbootsecurity.entities.Message;
import org.springframework.stereotype.Repository;

@Repository
public interface MessageRepository extends JpaRepository<Message, Integer> {

}
