package net.springbootsecurity.repositories;

import net.springbootsecurity.entities.Vacation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VacationRepository extends JpaRepository<Vacation, Integer> {
//    Vacation countByPaidIsTrue();
}
